function Animal(age) {
	this.age = age;
	this.sleep = function() {
		console.log("zzzzzz");
	};
}
function Flyer(age){
	Animal.call(this, age);
	this.fly = function(){
		console.log("whoohoo");
	};
}
function Carnivore(age){
	Animal.call(this, age);
	this.eat = function(){
		console.log("yummi");
	};
}
function Dolphin(age){
	Animal.call(this, age);
	this.swim = function() {
		console.log("splah");
	}
}
function Lion(age){
	Carnivore.call(this, age);
	this.roar = function() {
		console.log("wrrrrrr");
	};
}
function Eagle(age){
	Carnivore.call(this, age);
	Flyer.call(this, age);
}
function Bee(age){
	Flyer.call(this, age);
}

var dolphin = new Dolphin(3);
var lion = new Lion(6);
var eagle = new Eagle(1);
var bee = new Bee(0.1);

dolphin.sleep();
lion.eat();
console.log(eagle.age);
bee.fly();